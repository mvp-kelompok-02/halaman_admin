import React, { Component, Fragment } from 'react';

// * import Forlder Fitur
import Belum_Registrasi from '../Fitur/Belum_Registrasi';
import Hapus_User from '../Fitur/Hapus_User';
import Jumlah_Admin from '../Fitur/Jumlah_Admin';
import Top_Kontributor from '../Fitur/Top_Kontributor';

//css
import '../vendor/w3_css/color.css';
import '../vendor/w3_css/W3.CSS-framework.css';
import './Halaman_Admin.css';

export default class Halaman_Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNotRegist: false,
            isDeleteUser: false,
            sumAdmin: false,
            topContributor: false,
        };
    }

    handleDeleteUserChange(NewValue) {
        this.setState({ isDeleteUser: NewValue });
    }

    handleNotRegistChange(NewValue) {
        this.setState({ isNotRegist: NewValue });
    }

    handleSumAdminChange(NewValue) {
        this.setState({ sumAdmin: NewValue });
    }

    handleTopContributorChange(NewValue) {
        this.setState({ topContributor: NewValue });
    }

    // Toggle Menu Side Bar muncul /tampil, dan menembah overlay effect
    w3_open() {
        // Ambil Element Sidebar
        let mySidebar = document.getElementById('mySidebar');

        // Ambil Element DIV dengan overlay effect
        let overlayBg = document.getElementById('myOverlay');

        if (mySidebar.style.display === 'block') {
            mySidebar.style.display = 'none';
            overlayBg.style.display = 'none';
        } else {
            mySidebar.style.display = 'block';
            overlayBg.style.display = 'block';
        }
    }

    // Tombol Close Menu
    w3_close() {
        // Ambil Element Sidebar
        let mySidebar = document.getElementById('mySidebar');

        // Ambil Element DIV dengan overlay effect
        let overlayBg = document.getElementById('myOverlay');

        mySidebar.style.display = 'none';
        overlayBg.style.display = 'none';
    }

    render() {
        const section = {
            NotRegist: null,
            DeleteUser: null,
            SumAdmin: null,
            TopContributor: null,
        };

        if (this.state.isNotRegist) {
            section.NotRegist = (
                <Belum_Registrasi
                    onNotRegistChange={(value) => {
                        this.handleNotRegistChange(value);
                    }}
                />
            );
        }

        if (this.state.isDeleteUser) {
            section.DeleteUser = (
                <Hapus_User
                    onDeleteUserChange={(value) => {
                        this.handleDeleteUserChange(value);
                    }}
                />
            );
        }

        if (this.state.sumAdmin) {
            section.SumAdmin = (
                <Jumlah_Admin
                    onSumAdminChange={(value) =>
                        this.handleSumAdminChange(value)
                    }
                />
            );
        }

        if (this.state.topContributor) {
            section.TopContributor = (
                <Top_Kontributor
                    onTopContributorChange={(value) =>
                        this.handleTopContributorChange(value)
                    }
                />
            );
        }

        return (
            <Fragment>
                <div className="crock w3-light-grey">
                    <div
                        className="w3-bar w3-top w3-black w3-large"
                        style={{ zIndex: 4 }}
                    >
                        <button
                            className="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey"
                            onClick={() => this.w3_open()}
                        >
                            <i className="fa fa-bars"></i>  Menu
                        </button>
                        <a
                            style={{ textDecoration: 'none' }}
                            href="#"
                            className="w3-bar-item w3-right"
                        >
                            Log out
                        </a>
                    </div>

                    <nav
                        className="w3-sidebar w3-collapse w3-white w3-animate-left"
                        style={{ zIndex: 3, width: 300 }}
                        id="mySidebar"
                    >
                        <br />
                        <div className="w3-container w3-row">
                            <div className="w3-col s4">
                                <i className="fa fa-user-circle fa-2x"></i>
                            </div>
                            <br />
                            <div className="w3-col s8 w3-bar">
                                <span>
                                    Welcome, <b>Sulistio</b>
                                </span>
                            </div>
                            <br />
                            <hr style={{ marginTop: 30 }} />
                        </div>

                        <div className="w3-container">
                            <h5>Menu</h5>
                        </div>
                        <div className="w3-bar-block">
                            <a
                                href="#"
                                className="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black"
                                onClick={() => this.w3_close()}
                                title="close menu"
                            >
                                <i className="fa fa-remove fa-fw"></i>  Close
                                Menu
                            </a>
                            <a
                                href="#"
                                className="w3-bar-item w3-button w3-padding"
                            >
                                <i className="fa fa-home"></i>
                                <span>Home</span>
                            </a>
                            <a
                                href="#"
                                className="w3-bar-item w3-button w3-padding"
                            >
                                <i className="fa fa-lightbulb-o fa-fw"></i>
                                <span>Ide</span>
                            </a>
                            <a
                                href="#"
                                className="w3-bar-item w3-button w3-padding"
                            >
                                <i className="fa fa-users fa-fw"></i>
                                <span>Forum Diskusi</span>
                            </a>

                            <a
                                href="#"
                                className="w3-bar-item w3-button w3-padding"
                            >
                                <i className="fa fa-cog fa-fw"></i>  Settings
                            </a>
                            <br />
                            <br />
                        </div>
                    </nav>

                    {/* Overlay effect Ketika membuka sidebarr */}
                    <div
                        className="w3-overlay w3-hide-large w3-animate-opacity"
                        onClick={() => this.w3_close()}
                        style={{ cursor: 'pointer' }}
                        title="close side menu"
                        id="myOverlay"
                    ></div>

                    {/* ! Halaman Utama ! */}

                    <div
                        className="w3-main"
                        style={{ marginLeft: 300, marginTop: 43 }}
                    >
                        {/* Header */}
                        <header
                            className="w3-container"
                            style={{ paddingTop: 22 }}
                        >
                            <h3>
                                <b>Halaman Home</b>
                            </h3>
                        </header>

                        <hr className="menu" />

                        <br />
                        <div className="w3-row-padding w3-margin-bottom w3-center">
                            <div
                                className="w3-third crock1"
                                onClick={() => {
                                    this.setState({ isNotRegist: true });
                                }}
                            >
                                <div className="container_click w3-container w3-highway-red w3-padding-16 w3-round-large w3-margin-bottom">
                                    <div className="w3-center">
                                        <i className="fa fa-user-circle fa-2x w3-xxxlarge"></i>
                                    </div>

                                    <div className="w3-clear"></div>
                                    <h4>Belum registrasi</h4>
                                </div>
                            </div>

                            <div className="w3-third">
                                <div className="container_click w3-container w3-vivid-red w3-padding-16 w3-round-large w3-margin-bottom">
                                    <div className="w3-center">
                                        <i className="fa fa-user-circle fa-2x w3-xxxlarge"></i>
                                    </div>

                                    <div className="w3-clear"></div>
                                    <h4>Jumlah Grup</h4>
                                </div>
                            </div>

                            <div
                                className="w3-third"
                                onClick={() => {
                                    this.setState({ topContributor: true });
                                }}
                            >
                                <div className="container_click w3-container w3-red w3-padding-16 w3-round-large w3-margin-bottom">
                                    <div className="w3-center">
                                        <i className="fa fa-user-circle fa-2x w3-xxxlarge"></i>
                                    </div>

                                    <div className="w3-clear"></div>
                                    <h4>Top Kontributor</h4>
                                </div>
                            </div>
                        </div>

                        <div className="w3-row-padding w3-margin-bottom w3-center">
                            <div
                                className="w3-third"
                                onClick={() => {
                                    this.setState({ isDeleteUser: true });
                                }}
                            >
                                <div className="container_click w3-container w3-highway-green w3-padding-16 w3-round-large w3-margin-bottom">
                                    <div className="w3-center">
                                        <i className="fa fa-user-circle fa-2x w3-xxxlarge"></i>
                                    </div>

                                    <div className="w3-clear"></div>
                                    <h4>Hapus User</h4>
                                </div>
                            </div>

                            <div className="w3-third">
                                <div className="container_click w3-container w3-vivid-green w3-padding-16 w3-round-large w3-margin-bottom">
                                    <div className="w3-center">
                                        <i className="fa fa-user-circle fa-2x w3-xxxlarge"></i>
                                    </div>

                                    <div className="w3-clear"></div>
                                    <h4>Lihat Artikel</h4>
                                </div>
                            </div>

                            <div
                                className="w3-third"
                                onClick={() => {
                                    this.setState({ sumAdmin: true });
                                }}
                            >
                                <div className="container_click w3-container w3-vivid-yellowish-green w3-padding-16 w3-round-large w3-margin-bottom">
                                    <div className="w3-center">
                                        <i className="fa fa-user-circle fa-2x w3-xxxlarge"></i>
                                    </div>

                                    <div className="w3-clear"></div>
                                    <h4>Jumlah Admin</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    {section.NotRegist}
                    <br />
                    {section.DeleteUser}
                    <br />
                    {section.SumAdmin}
                    <br />
                    {section.TopContributor}

                    <br />
                    <br />
                </div>
            </Fragment>
        );
    }
}
