import React, { Component, Fragment } from 'react';

export default class Top_Kontributor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            topContributor: false,
        };
    }

    handleTopContributorChange(TopContributorValue) {
        this.props.onTopContributorChange(TopContributorValue);
    }

    handleCloseTopContributor() {
        this.setState({ topContributor: false }, () => {
            this.handleTopContributorChange(this.state.topContributor);
        });
    }

    render() {
        return (
            <Fragment>
                <div
                    className="w3-main w3-border w3-border-deep-purple w3-round-large"
                    style={{ marginLeft: 300 }}
                >
                    <div className="w3-row-padding">
                        <div className="w3-container w3-padding-32">
                            <div className="w3-display-container">
                                <i
                                    className="fa fa-remove fa-2x w3-transparent w3-display-right w3-text-red"
                                    style={{ cursor: 'pointer' }}
                                    onClick={() => {
                                        this.handleCloseTopContributor();
                                    }}
                                ></i>
                            </div>
                            <h2>Top Contributor</h2>

                            <ul className="w3-ul w3-card-4">
                                <li className="w3-display-container w3-padding">
                                    Fikri Al Hakim
                                </li>

                                <li className="w3-display-container">
                                    Ari Suseno
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
