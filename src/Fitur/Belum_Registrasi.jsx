import React, { Component, Fragment } from 'react';

//css
import '../vendor/w3_css/color.css';
import '../vendor/w3_css/W3.CSS-framework.css';

export default class Belum_Registrasi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNotRegist: false,
        };
    }

    handleNotRegistChange(notRegistValue) {
        this.props.onNotRegistChange(notRegistValue);
    }

    handleCloseNotRegist() {
        this.setState({ isNotRegist: false }, () => {
            this.handleNotRegistChange(this.state.isNotRegist);
        });
    }

    render() {
        return (
            <Fragment>
                <div
                    className="w3-main w3-border w3-border-blue-grey w3-round-large"
                    style={{ marginLeft: 300 }}
                >
                    <div className="w3-row-padding">
                        <div className="w3-container w3-padding-32">
                            <div className="w3-display-container">
                                <i
                                    className="fa fa-remove fa-2x w3-transparent w3-display-right w3-text-red"
                                    style={{ cursor: 'pointer' }}
                                    onClick={() => {
                                        this.handleCloseNotRegist();
                                    }}
                                ></i>
                            </div>
                            <h2>Belum Registrasi</h2>

                            <ul className="w3-ul w3-card-4">
                                <li className="w3-display-container ">
                                    Zufar{' '}
                                    <button className="w3-btn w3-display-right w3-green">
                                        Kirim
                                    </button>
                                    <br />
                                </li>

                                <li className="w3-display-container">
                                    Sulistio{' '}
                                    <button className="w3-btn w3-display-right w3-green">
                                        Kirim
                                    </button>
                                </li>

                                <li className="w3-display-container">
                                    Fikri{' '}
                                    <button className="w3-btn w3-display-right w3-green">
                                        Kirim
                                    </button>
                                </li>
                                <li className="w3-display-container">
                                    Aisyah{' '}
                                    <button className="w3-btn w3-display-right w3-green">
                                        Kirim
                                    </button>
                                </li>
                                <li className="w3-display-container">
                                    Adhi{' '}
                                    <button className="w3-btn w3-display-right w3-green">
                                        Kirim
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
