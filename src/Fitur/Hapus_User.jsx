import React, { Component, Fragment } from 'react';

export default class Hapus_User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDeleteUser: false,
        };
    }

    handleDeleteChange(deleteUserValue) {
        this.props.onDeleteUserChange(deleteUserValue);
    }

    handleCloseDelete() {
        this.setState({ isDeleteUser: false }, () => {
            this.handleDeleteChange(this.state.isDeleteUser);
        });
    }

    render() {
        return (
            <Fragment>
                <div
                    className="w3-main w3-border w3-border-deep-purple w3-round-large"
                    style={{ marginLeft: 300 }}
                >
                    <div className="w3-row-padding">
                        <div className="w3-container w3-padding-32">
                            <div className="w3-display-container">
                                <i
                                    className="fa fa-remove fa-2x w3-transparent w3-display-right w3-text-red"
                                    style={{ cursor: 'pointer' }}
                                    onClick={() => {
                                        this.handleCloseDelete();
                                    }}
                                ></i>
                            </div>
                            <h2>Hapus User</h2>

                            <ul className="w3-ul w3-card-4">
                                <li className="w3-display-container">
                                    Zufar{' '}
                                    <i
                                        className="fa fa-remove w3-transparent w3-display-right w3-text-red w3-margin-right"
                                        style={{ cursor: 'pointer' }}
                                    ></i>
                                </li>
                                <li className="w3-display-container">
                                    Sulistio{' '}
                                    <i
                                        className="fa fa-remove w3-transparent w3-display-right w3-text-red w3-margin-right"
                                        style={{ cursor: 'pointer' }}
                                    ></i>
                                </li>
                                <li className="w3-display-container">
                                    Fikri{' '}
                                    <i
                                        className="fa fa-remove w3-transparent w3-display-right w3-text-red w3-margin-right"
                                        style={{ cursor: 'pointer' }}
                                    ></i>
                                </li>
                                <li className="w3-display-container">
                                    Aisyah{' '}
                                    <i
                                        className="fa fa-remove w3-transparent w3-display-right w3-text-red w3-margin-right"
                                        style={{ cursor: 'pointer' }}
                                    ></i>
                                </li>
                                <li className="w3-display-container">
                                    Adhi{' '}
                                    <i
                                        className="fa fa-remove w3-transparent w3-display-right w3-text-red w3-margin-right"
                                        style={{ cursor: 'pointer' }}
                                    ></i>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
