import React from 'react';
import Halaman_Admin from './Pages/Halaman_Admin';
import { Fragment } from 'react';

export default function App() {
    return (
        <Fragment>
            <Halaman_Admin />
        </Fragment>
    );
}
